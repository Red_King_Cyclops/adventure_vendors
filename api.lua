adventure_vendors = {}

function adventure_vendors.register_vendor(name, desc, giveStackName, giveStackCount, receiveStackName, receiveStackCount)
    minetest.register_node("adventure_vendors:" .. name, {
        description = "Adventure Vendor\n" .. desc,
        tiles = {"adventure_vendor_vending_machine_face.png"},
        tiles = {
			"adventure_vendor_vending_machine_side.png",    -- y+
			"adventure_vendor_vending_machine_side.png",  -- y-
			"adventure_vendor_vending_machine_side.png", -- x+
			"adventure_vendor_vending_machine_side.png",  -- x-
			"adventure_vendor_vending_machine_side.png",  -- z+
			"adventure_vendor_vending_machine_face.png", -- z-
		},
        is_ground_content = false,
        groups = {cracky=3},
        paramtype2 = "facedir",
        on_rightclick = function(pos, node, player, itemstack, pointed_thing)
            if itemstack:get_name() == giveStackName and itemstack:get_count() >= giveStackCount then
				local inv = player:get_inventory()
				local taken = itemstack:take_item(giveStackCount)
				local added = inv:add_item("main", ItemStack({name = receiveStackName, count = receiveStackCount}))
				minetest.chat_send_player(player:get_player_name(), "Your purchase was successful.")
			else
				if itemstack:get_count() == 0 then
					minetest.chat_send_player(player:get_player_name(), desc)
				else
					minetest.chat_send_player(player:get_player_name(), "You are not holding the required item and/or you are not holding enough for the purchase.")
				end
			end
        end
	})
end

--[[
--------------------------------------------------

            --local inv = player:get_inventory()
            --local stack    = ItemStack("default:stone 99")
            --local taken = inv:remove_item("main", stack)
            --local stack2 = ItemStack("default:diamond 1")
            --local added = inv:add_item("main", stack2)

            --if itemstack:item_fits({name = "default:stone"}) then
				--itemstack:add_item({name = "default:stone"})

--Trash

            ------------------------------------------------------
            if(1==0) --trash
            then
            
            local inv = player:get_inventory()
			local notenough = false
			
            for i = 0,#givestacks - 1,1 
            do
				local givestack = ItemStack(givestacks[i])
				--local added = inv:set_stack("main", 0, givestack) --test
				if(not inv.contains_item("main", getstack))
				then
					notenough = true
                end
            end
            
            if(notenough == false)
            then
				for i = 0,#givestacks - 1,1
				do
					local givestack = ItemStack(givestacks[i])
					local taken = inv:take_item("main", givestack)
				end
				for i = 0,#receivestacks - 1,1
				do
					local receivestack = ItemStack(receivestacks[i])
					local added = inv:add_item("main", receivestack)
				end
				minetest.chat_send_player(player:get_player_name(), "Your purchase was successful.")
			else
				minetest.chat_send_player(player:get_player_name(), "You do not have all the required items for the purchase.")
            end
        
        end --trash end
]]
