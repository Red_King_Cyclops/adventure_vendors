# adventure_vendors

This mod adds in "adventure vending machines", which are vending machine blocks that have no owner and are intended for adventure maps instead of servers. In order to use an adventure vending machine, you must right click it while holding a certain stack of items to receive one or more new items. To find out what the machine buys and sells, right click the machine with an empty hand or create a sign next to it that explains what it sells and buys. To create an adventure vending machine, you must use the API and write/fill-in a function that adds in the adventure vending machine in "vendors.lua" or in another file in your own mod.

The code is licenced under LGPL 2.1 and the textures are licenced under CC-BY-SA. The code and textures were created by Red_King_Cyclops.