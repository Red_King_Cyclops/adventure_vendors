--params:
--adventure_vendors.register_vendor(TECHNICAL_NAME (string with no spaces), DESCRIPTION (string), ITEM_NAME_YOU_SELL (string with no spaces), NUMBER_OF_ITEM_NAME_YOU_SELL (integer), ITEM_NAME_YOU_BUY (string with no spaces), NUMBER_OF_ITEM_NAME_YOU_BUY (integer))

--Examples
if(minetest.get_modpath("default")) ~= nil then
    adventure_vendors.register_vendor("coal_lump_99_for_diamond_1", "Purchase 1 diamond for 99 coal lumps.", "default:coal_lump", 99, "default:diamond", 1)
    adventure_vendors.register_vendor("gold_ingot_5_for_diamond_sword_1", "Purchase 1 diamond sword for 5 gold ingots.", "default:gold_ingot", 5, "default:sword_diamond", 1)
    adventure_vendors.register_vendor("iron_ingot_2_for_dirt_99", "Purchase 99 blocks of dirt for 2 iron ingots.", "default:steel_ingot", 2, "default:dirt", 99)
end
--Add your own below (the entire point of this mod)
